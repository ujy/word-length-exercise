Library that finds the longest and shortest words
=================================================

Not the most creative name for a library, but it does the job.

Input: A sentence that uses the Latin alphabet.

Output: An object that contains the longest length found,
and the list of words with that length.

You have 3 versions to choose from:
* Version 1 uses a simple `for` loop and only saves the word(s)
with the longest/shortest length.
* Version 2 groups all words into a `Map` with the length used
as the key. The entry with the smallest (largest) key is the entry
that contains the shortest (longest) words.
* Version 3 is the fancy version of Version 1. It makes the text
into a `Stream` and uses a `Collector`. This probably has the
potential for parallelization to process a large corpus of text.

Limitations in this version:
* No numerals, e.g. '1', '90', etc. That also means "3M Company" is invalid.
* No special characters. Only A to Z (upper and lower case).
* No contractions or possessives, e.g. "isn't", "she's".
* Multi-word names would be broken down into individual words (no n-grams), e.g.
"John Smith" is not recognised as a single entity.
* That means this library cannot process "X Æ A-12" (sorry, Elon).

Example of building and running on the command line
---------------------------------------------------
Should you want to run this as an individual app, please run:

1. `./gradlew build`
2. `java -jar build/libs/word-length-exercise-1.0-SNAPSHOT.jar -l hello`

You should expect this output on your console:

```The longest word(s): [hello]; length: 5.```

To find the shortest word in a sentence, use the `-s` flag:
`java -jar build/libs/word-length-exercise-1.0-SNAPSHOT.jar -s Your sentence here.`

To find the shortest word in a sentence, use the `-l` flag:
`java -jar build/libs/word-length-exercise-1.0-SNAPSHOT.jar -l Your sentence here.`

The command line version uses Version 1.