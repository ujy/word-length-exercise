package com.example.word.length.exercise.version3;

import com.example.word.length.exercise.common.WordLength;
import com.example.word.length.exercise.testcommon.AbstractTest;

/**
 * See {@link AbstractTest} for test cases.
 */
public final class Version3Test extends AbstractTest {
    @Override
    protected WordLength longestWordsFunction(final String input) {
        return Version3.findLongestWord(input);
    }

    @Override
    protected WordLength shortestWordsFunction(final String input) {
        return Version3.findShortestWord(input);
    }
}
