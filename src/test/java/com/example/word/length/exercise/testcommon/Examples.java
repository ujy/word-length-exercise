package com.example.word.length.exercise.testcommon;

public final class Examples {

    public static final String SAMPLE_1_RAW_SENTENCE = "Lions, tigers, and bears! Oh my!";
    public static final String SAMPLE_1_NO_PUNCTUATION = "Lions tigers and bears Oh my";
    public static final String[] SAMPLE_1_LOWER_SPLIT = { "lions", "tigers", "and", "bears", "oh", "my" };

    public static final String SAMPLE_2_RAW_SENTENCE = "The cow jumped over the moon.";

    public static final String SAMPLE_3_RAW_SENTENCE = "Seeking universal agreement.";

}
