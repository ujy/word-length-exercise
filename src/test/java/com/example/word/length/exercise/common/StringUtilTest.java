package com.example.word.length.exercise.common;

import org.junit.Assert;
import org.junit.Test;

import static com.example.word.length.exercise.testcommon.Examples.*;

public final class StringUtilTest {

    @Test
    public void testRemoveAllNonLetters() {
        Assert.assertEquals(
                SAMPLE_1_NO_PUNCTUATION,
                StringUtil.removeAllNonLetters(SAMPLE_1_RAW_SENTENCE)
        );
    }

    @Test
    public void testFindAllIndividualWords() {
        Assert.assertArrayEquals(
                SAMPLE_1_LOWER_SPLIT,
                StringUtil.findAllIndividualWords(SAMPLE_1_RAW_SENTENCE)
        );
    }

}
