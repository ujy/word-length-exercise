package com.example.word.length.exercise.version1;

import com.example.word.length.exercise.common.WordLength;
import com.example.word.length.exercise.testcommon.AbstractTest;

/**
 * See {@link AbstractTest} for test cases.
 */
public final class Version1Test extends AbstractTest {

    @Override
    protected WordLength longestWordsFunction(final String input) {
        return Version1.findLongestWords(input);
    }

    @Override
    protected WordLength shortestWordsFunction(final String input) {
        return Version1.findShortestWords(input);
    }
}
