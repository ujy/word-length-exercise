package com.example.word.length.exercise.version2;

import com.example.word.length.exercise.common.WordLength;
import com.example.word.length.exercise.testcommon.AbstractTest;

/**
 * See {@link AbstractTest} for test cases.
 */
public final class Version2Test extends AbstractTest {
    @Override
    protected WordLength longestWordsFunction(final String input) {
        return Version2.findLongestWord(input);
    }

    @Override
    protected WordLength shortestWordsFunction(final String input) {
        return Version2.findShortestWord(input);
    }
}
