package com.example.word.length.exercise.testcommon;

import com.example.word.length.exercise.common.WordLength;
import org.junit.Test;

import java.util.Collections;

import static com.example.word.length.exercise.testcommon.Examples.SAMPLE_2_RAW_SENTENCE;
import static com.example.word.length.exercise.testcommon.Examples.SAMPLE_3_RAW_SENTENCE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Common testing class that all implementations of methods that find the longest and shortest
 * words can test against.
 */
public abstract class AbstractTest {

    protected abstract WordLength longestWordsFunction(String input);
    protected abstract WordLength shortestWordsFunction(String input);

    @Test
    public void testFindLongestWords() {

        assertEquals(
                new WordLength(6, Collections.singletonList("jumped")),
                longestWordsFunction(SAMPLE_2_RAW_SENTENCE)
        );

        final WordLength result = longestWordsFunction(SAMPLE_3_RAW_SENTENCE);
        assertEquals(9, result.getLength());
        assertEquals(2, result.getUniqueWords().size());
        assertTrue(result.getUniqueWords().contains("universal"));
        assertTrue(result.getUniqueWords().contains("agreement"));

    }

    @Test
    public void testFindShortestWords() {

        assertEquals(
                new WordLength(7, Collections.singletonList("seeking")),
                shortestWordsFunction(SAMPLE_3_RAW_SENTENCE)
        );

        final WordLength result = shortestWordsFunction(SAMPLE_2_RAW_SENTENCE);
        assertEquals(3, result.getLength());
        assertEquals(3, result.getWords().size());
        assertEquals(2, result.getUniqueWords().size());
        assertTrue(result.getUniqueWords().contains("the"));
        assertTrue(result.getUniqueWords().contains("cow"));

    }

}
