package com.example.word.length.exercise.common;

import java.util.*;

public final class WordLength {
    private final int length;
    private final List<String> words;

    public WordLength(final int length, final List<String> words) {
        this.length = length;
        this.words = new ArrayList<>(words);
    }

    public int getLength() {
        return length;
    }

    public List<String> getWords() {
        return new ArrayList<>(words);
    }

    public Set<String> getUniqueWords() {
        return new HashSet<>(words);
    }

    @Override
    public int hashCode() {
        return Objects.hash(length, words);
    }

    @Override
    public boolean equals(final Object other) {
        if (other != null) {
            if (other == this) {
                return true;
            } else if (other.getClass() == this.getClass()) {
                final WordLength o = (WordLength) other;
                return o.length == this.length && o.getWords().equals(this.getWords());
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("WordLength[length=%s, words=%s]", length, words);
    }
}
