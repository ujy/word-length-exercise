package com.example.word.length.exercise;


import com.example.word.length.exercise.common.WordLength;
import com.example.word.length.exercise.version1.Version1;

public final class Main {

    /**
     * Main method to demonstrate library from the command line.
     * Example usage: java -jar build/libs/word-length-exercise-1.0-SNAPSHOT.jar -l Your sentence here.
     * Use the '-l' flag to search for the longest words.
     * Use the '-s' flag to search for the shortest words.
     *
     * Uses Version 1 as it's the simplest.
     */
    public static void main(final String[] args) {

        if (args[0].toLowerCase().equals("-l")) {
            final WordLength wordLength = Version1.findLongestWords(collectWords(args));
            System.out.println(
                    String.format("The longest word(s): %s; length: %d.",
                            wordLength.getUniqueWords(), wordLength.getLength()
                    )
            );
        } else if (args[0].toLowerCase().equals("-s")) {
            final WordLength wordLength = Version1.findShortestWords(collectWords(args));
            System.out.println(
                    String.format("The shortest word(s): %s; length: %d.",
                            wordLength.getUniqueWords(), wordLength.getLength())
            );
        } else {
            System.out.println("Usage: ");
        }

    }

    private static String collectWords(final String[] words) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 1; i < words.length; i++) {
            sb.append(words[i]).append(" ");
        }
        return sb.toString();
    }

}
