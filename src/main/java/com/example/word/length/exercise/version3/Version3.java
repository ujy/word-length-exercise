package com.example.word.length.exercise.version3;

import com.example.word.length.exercise.common.WordLength;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;

import static com.example.word.length.exercise.common.StringUtil.findAllIndividualWords;

public final class Version3 {

    /**
     * Finds the longest word(s) in a string.
     * @param input your input string
     * @return an object that contains the maximum length and the list of words
     * with said length.
     */
    public static WordLength findLongestWord(final String input) {
        return findWordsBasedOnWordLength(input, findMaximum);
    }

    /**
     * Finds the shortest word(s) in a string.
     * @param input your input string
     * @return an object that contains the shortest length and the list of words
     * with said length.
     */
    public static WordLength findShortestWord(final String input) {
        return findWordsBasedOnWordLength(input, findMinimum);
    }

    private static WordLength findWordsBasedOnWordLength(final String input, final Predicate<Integer> isDesiredComparatorCondition) {
        return findWords(input, Comparator.comparingInt(String::length), isDesiredComparatorCondition);
    }

    private static WordLength findWords(
            final String input,
            final Comparator<String> individualWordComparator,
            final Predicate<Integer> isDesiredComparatorCondition) {
        final String[] inputStringAsArray = findAllIndividualWords(input);
        final List<String> result = Arrays.stream(inputStringAsArray)
                .collect(getOptimisingCollector(individualWordComparator, isDesiredComparatorCondition));
        return new WordLength(result.get(0).length(), result);
    }

    private static final Predicate<Integer> findMaximum = (comparatorResult) -> comparatorResult > 0;
    private static final Predicate<Integer> findMinimum = (comparatorResult) -> comparatorResult < 0;

    private static <T> Collector<T, List<T>, List<T>> getOptimisingCollector(final Comparator<T> elementComparator, final Predicate<Integer> isDesiredComparatorCondition) {
        return Collector.of(
                ArrayList::new,
                (collectingList, element) -> collectDesiredElementIntoList(collectingList, element, elementComparator, isDesiredComparatorCondition),
                (list1, list2) -> combineAllDesiredListsIntoTheMostDesiredList(list1, list2, elementComparator, isDesiredComparatorCondition));
    }

    private static <T> void collectDesiredElementIntoList(
            final List<T> collectingList,
            final T element,
            final Comparator<T> elementComparator,
            final Predicate<Integer> isDesiredComparatorCondition) {
        if (collectingList.isEmpty()) {
            collectingList.add(element);
        } else {
            final int comparisonResult = elementComparator.compare(element, collectingList.get(0));
            if (comparisonResult == 0) {
                collectingList.add(element);
            } else if (isDesiredComparatorCondition.test(comparisonResult)) {
                collectingList.clear();
                collectingList.add(element);
            }
        }
    }

    private static <T> List<T> combineAllDesiredListsIntoTheMostDesiredList(
            final List<T> list1,
            final List<T> list2,
            final Comparator<T> elementComparator,
            final Predicate<Integer> isDesiredComparatorCondition) {
        if (list1.isEmpty()) { return list2; }
        else if (list2.isEmpty()) { return list1; }
        else {
            final int comparisonResult = elementComparator.compare(list1.get(0), list2.get(0));
            if (comparisonResult == 0) {
                list1.addAll(list2);
                return list1;
            } else {
                return (isDesiredComparatorCondition.test(comparisonResult)) ? list1 : list2;
            }
        }
    }
}
