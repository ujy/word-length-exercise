package com.example.word.length.exercise.version1;

import com.example.word.length.exercise.common.WordLength;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

import static com.example.word.length.exercise.common.StringUtil.findAllIndividualWords;

public final class Version1 {

    /**
     * Finds the longest word(s) in a string.
     * @param input your input string
     * @return an object that contains the maximum length and the list of words
     * with said length.
     */
    public static WordLength findLongestWords(final String input) {
        return findWord(
                input,
                Integer.MIN_VALUE,
                (currentStringLength, currentMaximum) -> currentStringLength > currentMaximum
        );
    }

    /**
     * Finds the shortest word(s) in a string.
     * @param input your input string
     * @return an object that contains the shortest length and the list of words
     * with said length.
     */
    public static WordLength findShortestWords(final String input) {
        return findWord(
                input,
                Integer.MAX_VALUE,
                (currentStringLength, currentMinimum) -> currentStringLength < currentMinimum
        );
    }

    private static WordLength findWord(
            final String input,
            final int initialValue,
            final BiPredicate<Integer, Integer> condition) {
        final String[] inputStringAsArray = findAllIndividualWords(input);
        List<String> listOfTargetWords = new ArrayList<>();
        int mostExtremeLength = initialValue;
        for (final String string : inputStringAsArray) {
            if (string.length() == mostExtremeLength) {
                listOfTargetWords.add(string);
            } else if (condition.test(string.length(), mostExtremeLength)) {
                mostExtremeLength = string.length();
                listOfTargetWords = new ArrayList<>();
                listOfTargetWords.add(string);
            }
        }
        return new WordLength(mostExtremeLength, listOfTargetWords);
    }



    private Version1() {}
}
