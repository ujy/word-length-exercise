package com.example.word.length.exercise.common;

public final class StringUtil {

    public static String removeAllNonLetters(final String input) {
        return input.replaceAll("[^a-zA-Z ]", "");
    }

    public static String[] findAllIndividualWords(final String input) {
        return removeAllNonLetters(input).toLowerCase().split("\\s+");
    }

    // todo: handle contractions e.g. <word>'s, <word>n't
    // todo: handle words representing numbers e.g. sixty-two
    // todo: handle names like "3M Company", "X Æ A-12"
    // todo: handle urls like "https://www.google.com"

    private StringUtil() {}
}
