package com.example.word.length.exercise.version2;

import com.example.word.length.exercise.common.WordLength;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.example.word.length.exercise.common.StringUtil.findAllIndividualWords;
import static java.util.stream.Collectors.groupingBy;

public final class Version2 {

    /**
     * Finds the longest word(s) in a string.
     * @param input your input string
     * @return an object that contains the maximum length and the list of words
     * with said length.
     */
    public static WordLength findLongestWord(final String input) {
        return streamOfGroupedWordsByLength(input)
                .max(Comparator.comparingInt(Map.Entry::getKey))
                .map(Version2::convert)
                .orElse(null);
    }

    /**
     * Finds the shortest word(s) in a string.
     * @param input your input string
     * @return an object that contains the shortest length and the list of words
     * with said length.
     */
    public static WordLength findShortestWord(final String input) {
        return streamOfGroupedWordsByLength(input)
                .min(Comparator.comparingInt(Map.Entry::getKey))
                .map(Version2::convert)
                .orElse(null);
    }

    static Stream<Map.Entry<Integer, List<String>>> streamOfGroupedWordsByLength(final String input) {
        final String[] inputStringAsArray = findAllIndividualWords(input);
        return Arrays.stream(inputStringAsArray)
                .collect(groupingBy(String::length))
                .entrySet()
                .stream();
    }

    static WordLength convert(final Map.Entry<Integer, List<String>> entry) {
        return new WordLength(entry.getKey(), entry.getValue());
    }


    private Version2() {}
}
